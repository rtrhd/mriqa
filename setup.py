# -*- coding: utf-8 -*-
"""setup.py."""

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

from os.path import join, dirname

# Single definition of __version__ in version.py
with open(join(dirname(__file__), 'mriqa', 'version.py')) as f:
    exec(f.read())

config = {
    'name': 'mriqa',
    'description': 'Tools for MRI QA at UHB, NBT and CRICBristol',
    'version': __version__,
    'author': 'Ronald Hartley-Davies',
    'author_email': 'R.Hartley-Davies@physics.org',
    'license': 'MIT',
    'url': 'https://bitbucket.org/rtrhd/mriqa.git',
    'download_url': 'https://bitbucket.org/rtrhd/mriqa/get/v%s.zip' % __version__,
    'tests_require': ['nose'],
    'install_requires': [
        'numpy>=1.20.0',
        'scipy>=0.19.1',
        'matplotlib>=2.1.1',
        'pandas>=0.23.3',
        'xarray>=0.20.2',
        'scikit-image>0.14.0',
        'statsmodels>=0.12.2',
        'pydicom>=1.2.1',
        'dcmfetch>=0.3.0',
        'dcmextras>=0.3.0'
    ],
    'dependency_links': [
        'https://bitbucket.org/rtrhd/dcmfetch/get/v0.3.0.tar.gz#egg=dcmfetch-0.3.0',
        'https://bitbucket.org/rtrhd/dcmextras/get/v0.3.0.tar.gz#egg=dcmextras-0.3.0',
    ],
    'packages': ['mriqa', 'mriqa.xmlqa', 'mriqa.reports'],
    'package_data': {'mriqa': []},
    'scripts': [],
    'keywords': "mri qa phantom",
    'classifiers': [
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Science/Research',
        'Topic :: Scientific/Engineering :: Medical Science Apps.',
        'License :: OSI Approved :: MIT License',
        'Operating System :: POSIX',
        'Operating System :: Windows',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9'
    ],
}

setup(**config)
