from nose.tools import assert_almost_equal, eq_
import sys
from glob import glob

from pydicom import dcmread
import matplotlib as mpl

mpl.use("Agg")
sys.path.append('..')

from mriqa.tools import rx_coil_string
from mriqa.reports import snr

scans = {}


def assert_tuple_almost_equal(a, b, places=6):
    for a_i, b_i in zip(a, b):
        assert_almost_equal(a_i, b_i, places=places)


def setup():
    global scans
    scans['signal_combined_a'] = [dcmread(f) for f in glob('test-data/longbottle/signal_combined/a/*.dcm')]
    scans['signal_combined_b'] = [dcmread(f) for f in glob('test-data/longbottle/signal_combined/b/*.dcm')]
    scans['signal_per_element_a'] = [dcmread(f) for f in glob('test-data/longbottle/signal_per_element/a/*.dcm')]
    scans['signal_per_element_b'] = [dcmread(f) for f in glob('test-data/longbottle/signal_per_element/b/*.dcm')]
    scans['noise_combined_a'] = [dcmread(f) for f in glob('test-data/longbottle/noise_combined/a/*.dcm')]
    scans['noise_combined_b'] = [dcmread(f) for f in glob('test-data/longbottle/noise_combined/b/*.dcm')]
    scans['noise_per_element_a'] = [dcmread(f) for f in glob('test-data/longbottle/noise_per_element/a/*.dcm')]
    scans['noise_per_element_b'] = [dcmread(f) for f in glob('test-data/longbottle/noise_per_element/b/*.dcm')]


def teardown():
    pass


def test_snr_report():
    df_1 = snr.snr_report(scans['signal_combined_a'], scans['signal_combined_b'])
    df_2 = snr.snr_report(scans['signal_combined_a'] + scans['signal_combined_b'])
    assert all(s_1 == s_2 for s_1, s_2 in zip(df_1, df_2))
    coil = rx_coil_string(scans['signal_combined_a'][0])
    eq_(coil, 'Head_32')
    assert_tuple_almost_equal(df_1[coil], (422.0443844834035, 514.1391410142088))


def test_noise_correlation_report():
    df_1 = snr.noise_correlation_report(scans['noise_per_element_a'], scans['noise_per_element_b'])
    assert_almost_equal(df_1.MinCorrelation[0], -58.029201, 1)
    assert_almost_equal(df_1.MaxCorrelation[0], 4355.434911, 1)
    assert_almost_equal(df_1.DiagonalVariance[0], 0.007191, 3)
    assert_almost_equal(df_1.MaximumOffDiagonal[0], 0.239197, 3)


def test_noise_statistics_report_multichannel():
    df_1 = snr.noise_statistics_report_multichannel(scans['noise_per_element_a'])
    df_2 = snr.noise_statistics_report_multichannel(scans['noise_per_element_b'])
    eq_(df_1.Channels[0], 32)
    eq_(df_2.Channels[0], 32)
    assert_almost_equal(df_1.DegreesOfFreedom[0], 63.17, 1)
    assert_almost_equal(df_2.DegreesOfFreedom[0], 63.56, 1)
    assert_almost_equal(df_1.Scale[0], 70.82, 1)
    assert_almost_equal(df_2.Scale[0], 70.58, 1)


def test_noise_statistics_report_combined():
    df_1 = snr.noise_statistics_report_combined(scans['noise_combined_a'][0])
    df_2 = snr.noise_statistics_report_combined(scans['noise_combined_b'][0])
    assert_almost_equal(df_1.DegreesOfFreedom[0], 59.82, 1)
    assert_almost_equal(df_2.DegreesOfFreedom[0], 58.01, 1)
    assert_almost_equal(df_1.Scale[0], 73.05, 1)
    assert_almost_equal(df_2.Scale[0], 74.16, 1)


def test_snr_report_multi():
    df_1 = snr.snr_report_multi(scans['signal_per_element_a'], scans['signal_per_element_b'])
    coil = rx_coil_string(scans['signal_combined_a'][0])
    assert coil == 'Head_32'
    assert_tuple_almost_equal(df_1[coil], (422.2387292673857, 514.5961138829922))
