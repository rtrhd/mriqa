from nose.tools import *

from pydicom import dcmread

from mriqa import tools

import numpy as np

scans = {}


def assert_tuple_almost_equal(a, b):
    for a_i, b_i in zip(a, b):
        assert_almost_equal(a_i, b_i)


def setup():

    #global ge_scout, philips_se, philips_tse, philips_epi, philips_epse, siemens_scout
    global scans
    scans['ge_scout']      = dcmread('test-data/ge-scout.dcm')

    scans['philips_se']    = dcmread('test-data/philips-se.dcm')
    scans['philips_tse']   = dcmread('test-data/philips-tse.dcm')
    scans['philips_epi']   = dcmread('test-data/philips-epi.dcm')
    scans['philips_epse']  = dcmread('test-data/philips-epse.dcm')

    scans['siemens_scout'] = dcmread('test-data/siemens-scout.dcm')


def test_phantom_mask_2d():
    pass
    # phantom_mask_2d(imagei, dilate=False)

def test_ghost_mask():
    pass

def test_slice_ghostiness():
    pass
