from nose.tools import assert_true, assert_almost_equal, assert_raises, eq_

try:
    from pydicom import dcmread
except ImportError:
    from dicom import read_file as dcmread

from mriqa import tools, __version__

import numpy as np

scans = {}


def assert_tuple_almost_equal(a, b):
    for a_i, b_i in zip(a, b):
        assert_almost_equal(a_i, b_i)


def setup():

    global scans, ge_scans, philips_scans, siemens_scans
    scans['ge_scout']      = dcmread('test-data/ge-scout.dcm')
    scans['ge_memp']       = dcmread('test-data/ge-memp.dcm')
    scans['ge_fgre']       = dcmread('test-data/ge-fgre.dcm')
    scans['ge_efgre3d']    = dcmread('test-data/ge-efgre3d.dcm')

    scans['philips_se']    = dcmread('test-data/philips-se.dcm')
    scans['philips_tse']   = dcmread('test-data/philips-tse.dcm')
    scans['philips_epi']   = dcmread('test-data/philips-epi.dcm')
    scans['philips_epse']  = dcmread('test-data/philips-epse.dcm')
    scans['philips_std']   = dcmread('test-data/philips-se-std.dcm')

    scans['siemens_scout'] = dcmread('test-data/siemens-scout.dcm')
    scans['siemens_se']    = dcmread('test-data/siemens-se.dcm')
    scans['siemens_vb17_single'] = dcmread('test-data/siemens-vb17-single.dcm')
    scans['siemens_ve11_noise']  = dcmread('test-data/siemens-ve11-noise.dcm')
    scans['siemens_vb17_pca_phase']  = dcmread('test-data/siemens-vb17-pca-phase.dcm')
    scans['siemens_vb17_pca_magn'] = dcmread('test-data/siemens-vb17-pca-magn.dcm')
    scans['siemens_se_dis2d'] = dcmread('test-data/siemens-se-dis2d.dcm')
    scans['siemens_ve11_fid'] = dcmread('test-data/siemens-ve11-fid.dcm')
    scans['siemens_ve11_svs_se']  = dcmread('test-data/siemens-ve11-svs-se.dcm')
    scans['siemens_xa11a_enhanced_se'] = dcmread('test-data/siemens-xa11a-enhanced-se.dcm')
    scans['siemens_xa11a_interop_se'] = dcmread('test-data/siemens-xa11a-interop-se.dcm')


def teardown():
    pass


def test_all_ims():
    pass


def test_single_im():
    pass


def test_im_pair_1():
    pair = tools.im_pair(scans['siemens_scout'], scans['siemens_scout'])
    eq_(len(pair), 2)
    eq_(type(pair[0]), type(pair[1]))
    eq_(type(pair[0]), np.ndarray)
    eq_(pair[0].dtype, pair[1].dtype)
    eq_(pair[0].dtype, np.uint16)
    eq_(pair[0].shape, pair[1].shape)
    eq_(pair[0].shape, (512, 512))


def test_im_pair_2():
    assert_raises(ValueError, tools.im_pair, scans['siemens_scout'])


def test_im_pair_3():
    pair = tools.im_pair(scans['philips_epi'])
    eq_(len(pair), 2)
    eq_(type(pair[0]), type(pair[1]))
    eq_(type(pair[0]), np.ndarray)
    eq_(pair[0].dtype, pair[1].dtype)
    eq_(pair[0].dtype, np.uint16)
    eq_(pair[0].shape, pair[1].shape)
    eq_(pair[0].shape, (64, 64))


def test_im_pair_4():
    pair = tools.im_pair(scans['siemens_xa11a_interop_se'], scans['siemens_xa11a_interop_se'])
    eq_(len(pair), 2)
    eq_(type(pair[0]), type(pair[1]))
    eq_(type(pair[0]), np.ndarray)
    eq_(pair[0].dtype, pair[1].dtype)
    eq_(pair[0].dtype, np.uint16)
    eq_(pair[0].shape, pair[1].shape)
    eq_(pair[0].shape, (256, 256))


def test_im_pair_5():
    pair = tools.im_pair(scans['siemens_xa11a_enhanced_se'], scans['siemens_xa11a_enhanced_se'])
    eq_(len(pair), 2)
    eq_(type(pair[0]), type(pair[1]))
    eq_(type(pair[0]), np.ndarray)
    eq_(pair[0].dtype, pair[1].dtype)
    eq_(pair[0].dtype, np.uint16)
    eq_(pair[0].shape, pair[1].shape)
    eq_(pair[0].shape, (256, 256))


def test_mean_im():
    pass
def test_im_at_index():
    pass
def test_diff_im():
    pass
def test_snr_im():
    pass
def test_snr():
    pass
def test_uniformity_ipem80():
    pass
def test__mosaic_single():
    pass
def test_show_mosaic_single():
    # graphical - difficult to test
    pass
def test__mosaic():
    pass
def test_show_mosaic():
    # graphical - difficult to test
    pass
def test__basic_montage():
    pass
def test__philips_multiframe_montage():
    pass
def test__siemens_multiframe_montage():
    pass
def test__montage():
    pass
def test_show_montage():
    # graphical - difficult to test
    pass
def test_normalized_profile():
    pass
def test_profile_params():
    pass
def test_peakdet():
    pass
def test__gaussian_fit():
    pass
def test__refine_peak():
    pass
def test_positive_gradient():
    pass
def test_radial_profiles():
    pass
def test_profile_edges():
    pass
def test_profile_diameters():
    pass
def test_fit_diameters():
    pass
def test_edges_and_diameters():
    pass
def test_naad():
    pass
def test_image_uniformity_nema():
    pass
def test_uniformity_nema():
    pass


def test_watermark():
    wm = tools.watermark()
    expected_keys = [
        'CalculationTime', 'User',
        'PythonVersion', 'Platform',
        'mriqa', 'dcmextras', 'pydicom', 'scipy', 'numpy', 'skimage', 'matplotlib'
    ]
    assert all(k in wm for k in expected_keys)
    assert all(wm.values())
    eq_(wm['mriqa'], __version__)


def test_rx_coil_string():
    eq_(tools.rx_coil_string(scans['ge_scout']), 'C-GE_HEAD')
    eq_(tools.rx_coil_string(scans['ge_memp']), 'C-GE_HNS Head')
    eq_(tools.rx_coil_string(scans['ge_fgre']), 'C-GE_HNS Head')
    eq_(tools.rx_coil_string(scans['ge_efgre3d']), 'C-GE_HNS Head')
    # TODO: Really want to try harder to get something sensible for philips.
    eq_(tools.rx_coil_string(scans['philips_se']), 'MULTI COIL')
    eq_(tools.rx_coil_string(scans['philips_tse']), 'MULTI COIL')
    eq_(tools.rx_coil_string(scans['philips_epi']), 'MULTI COIL')
    eq_(tools.rx_coil_string(scans['philips_epse']), 'MULTI COIL')
    eq_(tools.rx_coil_string(scans['siemens_scout']), 'Head_32')
    eq_(tools.rx_coil_string(scans['siemens_se']), 'Head_32')
    eq_(tools.rx_coil_string(scans['siemens_vb17_single']), 'HeadMatrix')
    eq_(tools.rx_coil_string(scans['siemens_ve11_noise']), 'HeadNeck_20')
    eq_(tools.rx_coil_string(scans['siemens_vb17_pca_phase']), 'C:BO1,2;SP5-7')
    eq_(tools.rx_coil_string(scans['siemens_vb17_pca_magn']), 'C:BO1,2;SP5-7')
    eq_(tools.rx_coil_string(scans['siemens_ve11_fid']), 'HeadNeck_20')
    eq_(tools.rx_coil_string(scans['siemens_ve11_svs_se']), 'HeadNeck_20')
    eq_(tools.rx_coil_string(scans['siemens_xa11a_enhanced_se']), 'HeadNeck_20_TCS')
    eq_(tools.rx_coil_string(scans['siemens_xa11a_interop_se']), 'HeadNeck_20_TCS')
    eq_(tools.rx_coil_string(scans['siemens_xa11a_interop_se'], 'OVERRIDE'), 'OVERRIDE')
