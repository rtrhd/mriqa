.PHONY: install html tests clean
install: mriqa/dcmio.py  mriqa/ghosting.py  mriqa/__init__.py mriqa/phantoms.py  mriqa/tools.py
	python setup.py build --build-base=/tmp/mriqa-build install

html:
	/usr/local/anaconda/bin/jupyter nbconvert --to html --template full *.ipynb

tests:
	cd tests/test-data/fbirn; make
	cd tests/test-data/noras; make
	cd tests/test-data/diffusion; make
	cd tests; python3 -m pytest -s

clean:
	rm -rf *.html ipynb/*.html mriqa/*.pyc build dist mriqa.egg-info
	cd tests/test-data/fbirn make clean
