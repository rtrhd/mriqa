/*************************************************************************
 *  Compilation:  javac SE_MTF_2xNyquist.java
 *  Execution:    java SE_MTF_2xNyquist
 *
 *  Calculates the Modulation Transfer Function (MTF) for an image of f rows and c columns of pixels selected. 
 *  For FFT requirements, uses a sample of power of 2 rows for both the calculation of the MTF and the SPP.
 *  
 *  Calcula la Funcion de Transferencia de Modulacion (MTF) para una imagen de un tamano f filas y 
 *  c columnas de pixels seleccionados. 
 *  Por requisitos de la fft, se utiliza una muestra de hileras potencia de 2 tanto para el calculo 
 *  de la MTF como de la SPP.
 *  
 *  Limitations
 *  -----------
 *   * TIFF image only
 *  
**************************************************************************/

import ij.*;
import ij.process.*;
import ij.gui.*;
import java.awt.*;
import ij.plugin.*;
import java.awt.event.*;
import ij.plugin.PlugIn;
import ij.plugin.frame.*;
import ij.measure.*;
import ij.io.*;
import java.math.BigDecimal;
import ij.text.*;
import java.util.*;
import java.lang.Object.*;
import java.lang.Integer;
import ij.process.ColorProcessor;
import ij.process.ImageConverter;
import ij.process.StackConverter;
import ij.plugin.filter.*;
import ij.plugin.filter.RGBStackSplitter;
import ij.*;
import ij.process.*;
import ij.gui.*;
import ij.measure.Calibration;
import java.awt.*;
import ij.plugin.FFT.*;
import java.util.EventObject;

public class SE_MTF_2xNyquist implements PlugIn, ActionListener, WindowListener, DialogListener {
    Frame frame;
    ImagePlus imp, impOriginal;
    ProfilePlot plotESF;
    Plot plotResult;
    int k;
    int i;
    int selecWidth;
    int selecHeight;
    int sppLength;
    double[] ESFLinea;
    double[][] ESFArray;
    double[][] LSFArray;
    double[] Vector;
    double[][] Array;
    double[][] ArrayF;
    double[][] ESFArrayF;
    double[][] LSFArrayF;
    double[][] PosMax;
    double[]ESFVector;
    double[]LSFVector;
    double[]LSFDVector;
    double[]MTFVector;
    double[]SPPVector;
    double[]Max;
    String title;
    Roi roi;
    int optWidth;
    int optHeight;
    int sChannel;
    int sFrequnits;
    int type;
    boolean isStack;
    int roiType;
    int sSize;
    boolean cancel;
    int bit;
    int yMax;
    double mmSensors=0.0;
    int nPhotodetectors=0;
    double ny=1;

    // Message that asks the user what to do   
    public void run(String arg) {    
        frame = new Frame("SE_MTF");
        frame.setLayout(new GridBagLayout());
        frame.setBounds(100, 100, 450, 100);
        Button button1 = new Button("Generate MTF");
        button1.addActionListener(this);
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 1;
        c.gridy = 0;
        c.gridwidth = 1;
        c.gridwidth = 1;
        c.anchor = GridBagConstraints.NORTH;
        c.insets.top = 0;
        c.insets.left = 0;
        c.insets.bottom= 0;
        c.insets.right = 0;
        c.weightx = 0;
        c.weighty = 0;
        c.fill = GridBagConstraints.NONE;
        ((GridBagLayout)frame.getLayout()).setConstraints(button1, c);
        frame.add(button1);
        frame.validate();   
        frame.show();
        frame.setBackground(Color.lightGray);
        frame.setResizable(false);
        frame.setLocation(0,0);
        frame.addWindowListener(this);
    }   
    
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        if (command==null) {
            return;
        }
        if (command.equals("Generate MTF")) {
            cancel=false;
            openImage();
            
            if (cancel==false) {
                options();      
            }
            if (cancel==false) {

                generateESFArray("ESF Plot",imp,roi);
                generateLSFArray("LSF Plot",ESFArray);
                calculateMax();

                ESFArrayF=alignArray(ESFArray);

                if (cancel==false) {
                    LSFArrayF=alignArray(LSFArray);
                }
                if (cancel==false) { 
                    ESFVector=averageVector(ESFArrayF);
                }
                if (cancel==false) { 
                    LSFVector=averageVector(LSFArrayF);
                    
                    int aura = (LSFVector.length * 2);
                    LSFDVector = new double [aura];
                    int j = 0;
                    int aura2 = (LSFVector.length);
                    
                    for(i=0;i<(LSFDVector.length-3); i++) {
                        
                        if(i % 2 == 0) { 
                            LSFDVector[i]= LSFVector[j];
                            j=j+1;
                        }else {
                            LSFDVector[i]= ((0.375*LSFVector[(j-1)]) + (0.75*LSFVector[(j)]) - (0.125*LSFVector[(j+1)]));
                        }
                    }  
                    
                    LSFDVector[i] = ((LSFVector[j-1] + LSFVector[j])*0.5);
                    LSFDVector[i+1] = LSFVector[j];
                    LSFDVector[i+2] = LSFVector[j];
            
                    int indexMax = 0;
                    double valorMax = LSFDVector[0];
                    for(int i=0;i<LSFDVector.length;i++) {
                      if(valorMax < LSFDVector[i]) {
                          indexMax = i;
                          valorMax = LSFDVector[i];
                       }
                    }
                    i=indexMax;
                    LSFDVector[i-1]=((LSFDVector[i-2] + LSFDVector[i])*0.5);
                    
                    MTFVector=fftConversion(LSFDVector, "MTF");
                    Max=obtenerMax();
                    SPPVector=fftConversion(Max,"SPP");
                    
                    //-*****-S'hauria d'intentar que no quedessin superposats, sino en escala, cada un 10 mes avall
                    generatePlot(MTFVector,"MTF");
                    generatePlot(LSFVector,"LSF");
                    generatePlot(ESFVector,"ESF");
                    generatePlot(SPPVector,"SPP");
                    
                }
            }
            cleanImage();
        }
    }
    
    public void openImage() {
        boolean opButton=true;
        
        imp = WindowManager.getCurrentImage();
                
        if (imp==null) {
            IJ.showMessage("Error","There are no images open\nProcess canceled");
            cancel=true;
        }
        
        title=imp.getTitle();
        
        if (cancel==false) {
            type=imp.getType();
            switch(type) {
                case ImagePlus.GRAY8:{  
                    isStack=false;
                    yMax=255;
                } break;
                case ImagePlus.GRAY16:{
                    isStack=false;
                    yMax=65535;
                } break;
                case ImagePlus.GRAY32:{
                    isStack=false;
                    yMax=2147483647;
                } break;
                case ImagePlus.COLOR_256:{
                    isStack=true;
                    yMax=255;
                } break;
                case ImagePlus.COLOR_RGB:{
                    isStack=true;
                    yMax=255;
                } break;
                default: yMax=255;
            }
        
 
            //Get the selection
            roi = imp.getRoi();     
            if (roi==null) {
                imp.setRoi(0, 0, imp.getWidth(), imp.getHeight());
                roi = imp.getRoi();
                opButton=IJ.showMessageWithCancel("Warning","All image selected");
                if (opButton==false) cancel=true;
            }
        }
        
        if (cancel==false) {
            //Rectangular selection
            roiType = roi.getType();
            if (!(roi.isLine() || roiType==Roi.RECTANGLE)) {
                IJ.showMessage("Error","Line or rectangular selection required\nProcess canceled");
                cancel=true;
            }
        }
    }
    
    void cleanImage() { imp.killRoi(); }
 
    void generateStack() {
        if (isStack==true && sChannel!=3) {
            ImageConverter ic = new ImageConverter(imp);
            ic.convertToRGBStack();
            ImageStack is;
            is=imp.getStack();
        }
    }
        
    
    void options() {
        GenericDialog gd = new GenericDialog("MTF - Options",frame);
                
        //User can choose the units
        gd.addDialogListener(this);
        String[]frequnits=new String[3];
        frequnits[0]="Absolute (lp/mm)";
        frequnits[1]="Relative (cycles/pixel)";
        frequnits[2]="Line widths per picture height (LW/PH)";
        gd.addChoice("Frequency units:",frequnits,frequnits[1]);
        
        //Input data
        gd.addNumericField("Sensor size (mm): ",mmSensors,1);
        gd.addNumericField("Number of photodetectors: ",nPhotodetectors,0);
        ((Component) gd.getNumericFields().get(0)).setEnabled(false);
        ((Component) gd.getNumericFields().get(1)).setEnabled(false);
        
        //The user can choose the sample width
        String[]sampleSize=new String[5];
        sampleSize[0]="32";
        sampleSize[1]="64";
        sampleSize[2]="128";
        sampleSize[3]="256";
        sampleSize[4]="512";
        gd.addChoice("Sample size (pixels):",sampleSize,sampleSize[0]);             
        
        // If is a greyscale image there is no options avaliable
        if (isStack==false) {
            gd.addMessage("This is a greyscale image, no options avaliable");
        } else {
            // If is a RGB image, user can choose each channel or the channels average to calculate MTF
            gd.addMessage("This is a three channel image, select an option");
            String[]channel=new String[4];
            channel[0]="Red Channel";
            channel[1]="Green Channel";
            channel[2]="Blue Channel";
            channel[3]="Channels average";
            gd.addChoice("Channel",channel,channel[3]);
        }       
        
        //Show General Dialog (MTF Options)
        gd.showDialog();
        
        //Ends the proccess
        if (gd.wasCanceled()) {  
            cancel=true;
            cleanImage();
            return;
        }    
        
        gd.addDialogListener(this);
    
        //Set the stat of the NumericText
        mmSensors = (double)gd.getNextNumber();
        nPhotodetectors = (int)gd.getNextNumber();
        sFrequnits=gd.getNextChoiceIndex();
    
        //Frequency units
        if(sFrequnits==0) {
            ny = (nPhotodetectors/mmSensors);
        }
        if(sFrequnits==1) {
            ny = 1;
        }
        if(sFrequnits==2) {
            ny = (nPhotodetectors*2);
        }

        //Save options
        sSize=gd.getNextChoiceIndex();
        String stringSize=sampleSize[sSize];
        sSize=Integer.parseInt(stringSize);
        
        if (isStack==false) {
            sChannel=0;
        } else {
            sChannel=gd.getNextChoiceIndex();
        }       
                
        if(isStack==true && sChannel!=3) {
            generateStack();
            imp.setSlice(sChannel+1);
        }
    }
        
    public boolean dialogItemChanged(GenericDialog gd, AWTEvent e) {       
        sFrequnits=gd.getNextChoiceIndex();
        if(sFrequnits==1) {
            ((Component) gd.getNumericFields().get(0)).setEnabled(false);
            ((Component) gd.getNumericFields().get(1)).setEnabled(false);
        }
        if(sFrequnits==0) {
             ((Component) gd.getNumericFields().get(0)).setEnabled(true);
             ((Component) gd.getNumericFields().get(1)).setEnabled(true);
        }
        if(sFrequnits==2) {
            ((Component) gd.getNumericFields().get(0)).setEnabled(false);
            ((Component) gd.getNumericFields().get(1)).setEnabled(true);
        }
            
        return true;
    }
        
    
    //The grey values of the line selections are tipped in a Array
    void generateESFArray(String title, ImagePlus imp, Roi roi) {   
        Rectangle r=roi.getBounds();
        selecWidth=r.width;
        selecHeight=r.height;
                
        if (sSize>=selecWidth) {
            IJ.showMessage("Error","sample size is bigger than selection width\nProcess canceled");
            return;
        }
        
        int selectX=r.x;
        int selectY=r.y;
        int selectXFin=selectX+selecWidth;
        int selectYFin=selectY+selecHeight;
        ESFLinea=new double[selecWidth];
        ESFArray=new double[selecHeight][selecWidth];
        for(k=0;k<selecHeight;k++) {
            //select line 
            IJ.makeLine(selectX,k+selectY,selectXFin-1,k+selectY);
            //save values 
            plotESF = new ProfilePlot(imp);
            ESFLinea=plotESF.getProfile();
            //Load Array ESF
            for(i=0;i<selecWidth;i++) {
                ESFArray[k][i]=ESFLinea[i];
            }
        }
    }
    

    void generateLSFArray(String title, double[][]ESFArray) {
        LSFArray=new double[selecHeight][selecWidth];       
        for(k=0;k<selecHeight;k++) {
            for(i=0;i<selecWidth-1;i++) {
                LSFArray[k][i]=ESFArray[k][i+1]-ESFArray[k][i];
            }
        }
    }

 
    public double[][] alignArray(double[][] Array) {
        ArrayF = new double[selecHeight][sSize];
        int ini;
        int fin;
        
        //Create new array aligned
        for(k=0;k<selecHeight;k++) {
            //Initial and end positions of k-line
            ini=(int)PosMax[k][2];
            fin=(int)PosMax[k][3];
            
            for(i=ini;i<fin;i++) {                   
                ArrayF[k][i-ini]=Array[k][i];
            }
        }
        //final array - from 32 to 512 positions
        return ArrayF;
    }
    

    //Calculate maximum value and find 32 positions to align
    void calculateMax() {
        PosMax=new double[selecHeight][4] ;
        int posMax;
        int halfSize;
        halfSize=sSize/2;
        for(k=0;k<selecHeight-1;k++) {
            posMax=0;
            for(i=0;i<selecWidth-1;i++) {
                if (LSFArray[k][posMax]<LSFArray[k][i] || LSFArray[k][posMax]==LSFArray[k][i]) {
                    posMax=i;
                }
            }
            
            //The maximum value position on the line
            PosMax[k][0]=posMax;
            //The maximum value on the line
            PosMax[k][1]=LSFArray[k][posMax];
            //Starting and ending position to align maximum values
            PosMax[k][2]=PosMax[k][0]-halfSize;
            PosMax[k][3]=PosMax[k][0]+halfSize;
        }
    }

 
    public double[] averageVector(double[][] Array) {
        double result;
        int j;
        double[]Vector = new double[sSize];
        
        //Average of all linear ESF/LSF     
        for(i=0;i<sSize;i++) {
            result=0;
            //Average of all rows i-position  
            for(k=0;k<selecHeight;k++) { 
                result=result+Array[k][i];  
            }   
            result=result/selecHeight;
            Vector[i]=result;
        }
        return Vector;
    }
    
    
    public int longPotDos(int length) {
        int N;
        double log = Math.log(length)/Math.log(2);
        N=(int)Math.pow(2,(int)Math.floor(log));
        return N;
    }
    
    
    public double[] obtenerMax() {
        int N=longPotDos(selecHeight);
        Max=new double[N];
        for(k=0;k<N;k++) {
            Max[k]=PosMax[k][1];
        }
        return Max;
    }

    
    void generatePlot(double[] Vector, String plot) {
        double[]xValues;
        String ejeX="pixel";
        String ejeY="";
        String allTitle="";
        ImageProcessor imgProc;
        
        //If MTF plot, calculate the scale of cycles per pixel for x-axis values
        xValues=calculateXValues(Vector,plot);
        
        //plot titles       
        if (plot=="ESF") {
            ejeY="Grey Value";
        }

        if (plot=="LSF") { 
            ejeY="Grey Value / pixel";
        }
        
        if (plot=="MTF") {
            ejeY="Modulation Factor";
            
            //Units
            if(sFrequnits==0) {
            ejeX ="lp/mm";
            }
            if(sFrequnits==1) {
            ejeX ="Cycles/Pixel";
            }
            if(sFrequnits==2) {
            ejeX ="Line Width/Picture Height";
            }
        }
        
        if (plot=="SPP") {
            ejeY="SPP";
        }

        allTitle=plot + "_" + title;        
        plotResult = new Plot(allTitle, ejeX, ejeY, xValues, Vector);
        
        //plot limits
        if (plot=="ESF") {
            plotResult.setLimits(1,Vector.length,0,yMax);
        }
        
        if (plot=="LSF") {
            plotResult.setLimits(1,Vector.length,0,yMax);
        }
        
        if (plot=="MTF") {
            plotResult.setLimits(0,ny,0,1);
        }
                
        if (plot=="SPP") {
            plotResult.setLimits(1,Vector.length,0,1);
        }
         
        plotResult.draw();
        plotResult.show();
    }
    

    public double[] calculateXValues(double[] Vector, String plot) {
        int N=Vector.length;
        double[]xValues = new double[N];
        
        if(plot=="MTF") {
            xValues[0]=0;
            //Scale of values for x-axis
            for(i=1;i<N;i++) {
                //xValues[i]=xValues[i-1]+(0.5/(N-1));
                xValues[i]=xValues[i-1]+(ny/(N-1));
            }
        } else {
            for(i=0;i<N;i++) { 
                xValues[i]=i+1;
            }
        }
        return xValues;
    }
    
    
    //data type conversion from complex to double, to implement fft
    public double[] fftConversion(double[] Vector, String plot) {
        //Only half are necessary
        int N=Vector.length;
        int M=Vector.length/2;
        double divisor;
        Complex[] ArrayComplex = new Complex[N];
        Complex[] VectorFFTC = new Complex[N];
        double[]VectorFFTD=new double[M];
                        
        for (i = 0; i < N; i++) { 
            //A double array is converted into a complex array ; imaginary part=0           
            ArrayComplex[i] = new Complex(Vector[i], 0); 
        }
        //FFT operation
        VectorFFTC = fft(ArrayComplex); 
        
        if(plot=="SPP") {
            //Reject the first one
            for (i = 1; i < M; i++) {
                //absolute value (module)
                VectorFFTD[i-1]=VectorFFTC[i].abs()/VectorFFTC[1].abs();
            }
        } else {
            for (i = 0; i < M; i++) {
                //absolute value (module)
                VectorFFTD[i]=VectorFFTC[i].abs()/VectorFFTC[0].abs(); 
            }
        }
        
        //Normalize
        if(plot=="SPP") {
            divisor=findMaxSPP(VectorFFTD);
        } else {          
            divisor=VectorFFTD[0];  
        }
        
        for (i = 0; i < M; i++) {
            VectorFFTD[i]=VectorFFTD[i]/divisor; 
        }
        return VectorFFTD;
    }
    
    
    public double findMaxSPP(double[] Vector) {
        double max = 0;
        double value;
        for (int i=0; i<Vector.length; i++) {
            if (Vector[i]>max) max=Vector[i];
        }
        
        return max;
    }
    

    public class Complex {
        private final double re;
        private final double im;

        // from real and imaginary parts
        public Complex(double real, double imag) {
            this.re = real;
            this.im = imag;
        }
        
        // string representation
        public String toString()  {return re + " + " + im + "i"; }
        
        // (this + b)
        public Complex plus(Complex b) { 
            Complex a = this;             // invoking object
            double real = a.re + b.re;
            double imag = a.im + b.im;
            Complex sum = new Complex(real, imag);
            return sum;
        }
        // (this - b) 
        public Complex minus(Complex b) { 
            Complex a = this;   
            double real = a.re - b.re;
            double imag = a.im - b.im;
            Complex diff = new Complex(real, imag);
            return diff;
        }       
        
        // (this * b)
        public Complex times(Complex b) {
            Complex a = this;
            double real = a.re * b.re - a.im * b.im;
            double imag = a.re * b.im + a.im * b.re;
            Complex prod = new Complex(real, imag);
            return prod;
        }
        
        // |this|
        public double abs() { return Math.hypot(re, im); }
    }   
    
    
    public Complex[] fft(Complex[] x) {
        int N = x.length;
        Complex[] y = new Complex[N];

        // base case
        if (N == 1) {
            y[0] = x[0];
            return y;
        }

        // radix 2 Cooley-Tukey FFT
        if (N % 2 != 0) throw new RuntimeException("N is not a power of 2");
        Complex[] even = new Complex[N/2];
        Complex[] odd  = new Complex[N/2];
        for (int k = 0; k < N/2; k++) even[k] = x[2*k];
        for (int k = 0; k < N/2; k++) odd[k]  = x[2*k + 1];

        Complex[] q = fft(even);
        Complex[] r = fft(odd);

        for (int k = 0; k < N/2; k++) {
            double kth = -2 * k * Math.PI / N;
            Complex wk = new Complex(Math.cos(kth), Math.sin(kth));
            y[k]       = q[k].plus(wk.times(r[k]));
            y[k + N/2] = q[k].minus(wk.times(r[k]));
        }
        return y;
    }


    public void windowOpened(WindowEvent e) {}
    public void windowActivated(WindowEvent e) {}
    public void windowDeactivated(WindowEvent e) {}
    public void windowIconified(WindowEvent e) {}
    public void windowDeiconified(WindowEvent e) {}
    public void windowClosing(WindowEvent e) { frame.dispose(); }
    public void windowClosed(WindowEvent e) {}
}
