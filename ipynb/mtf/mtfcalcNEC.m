%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This code is adapted from that presented in

% S. N. Friedman, G. S. K. Fung, J. H. Siewerdsen, and B. M. W. Tsui.  "A
% simple approach to measure computed tomography (CT) modulation transfer 
% function (MTF) and noise-power spectrum (NPS) using the American College 
% of Radiology (ACR) accreditation phantom," Med. Phys. 40, 051907-1 -  
% 051907-9 (2013).
% http://dx.doi.org/10.1118/1.4800795
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all;          %close all figures to avoid confusion with previous runs
clear all;          %clear all variables in order to ensure smooth running of the programme
clc;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% User selected options
%

selectdir = 1;   % 1 = prompt for directly locations via dialogue box, 0 = hardcode locations below
    datadir = fullfile('data'); 
    resultsdir = fullfile('results',datadir);

saveresults = 1;  % Choose whether to save the calculated MTF to disk in text files

showfig = 4;  % decide how important a figure should be in order to be plotted, 1 = no figures, 4 = all figures

rstep = 0.1; %  Choose bin size in mm (i.e., effective pixel size) for oversampled edge spread function (ESF)

window = 1;  % Decide whether to apply a Hann window to the LSF before calculating the MTF (typically applied)

% Background and foreground values:  1 = prompted to draw rectangle to select ROI
manualROI = 1; 

% 2D map of good pixels (1) and bad pixels (0) to use in calculation
% 1 = prompted to draw rectangle(s) to select ROI(s). 
manualmask = 1;


% Determine which angles of data to use in radians (-pi to pi to use all)
thlim1 = -pi;
thlim2 = pi;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Start of analysis code

homedr = cd;                    %select the data and results directories

if selectdir ==1
    fprintf('Select the data directory.\n\n');
    datadir = uigetdir(homedr,'Pick data directory');
    if saveresults == 1
        fprintf('Select the results directory.\n\n');
        resultsdir = uigetdir(homedr,'Pick results directory');
    end
else       
    datadir = fullfile(homedr,datadir);    
    resultsdir = fullfile(homedr,resultsdir);
end

if saveresults == 1                 %if requested define results directory
    mkdir(resultsdir)
end

cd(datadir)

scandir = dir;

cd(homedr)

while or(strcmp(scandir(1).name,'.'),strcmp(scandir(1).name,'..'))
    scandir = scandir(2:end);
end

Nscan = size(scandir,1);   %Nscan = number of datasets within the scan directory
%Nscan = 1;                %Error leftover from debugging attempt??? -made the
                           %programme only look at one set

nfilescheck = 0;            %variable for counting the number of files 

for i = 1:Nscan             %look in all folders
    cd(fullfile(datadir,scandir(i).name))                       
    
    ffiles = dir;           % get list of filenames
    
    cd(homedr)
    
    while or(strcmp(ffiles(1).name,'.'),strcmp(ffiles(1).name,'..'))
        ffiles = ffiles(2:end);
    end
    
    Nz = size(ffiles,1);        %number of slices in each folder
    
     if nfilescheck ~= Nz && nfilescheck ~=0
        fprintf('Error.  Scan directories do not contain the same number of slices.\n')
        return
    end
    
    nfilescheck = Nz;
    
    if i == 1                   %Define sizes based on images in first dataset during first loop
        
        % Read DICOM header info and determine correct mapping function to get HU
        % values as well as voxel sizes.
        
        im = single(dicomread(fullfile(datadir,scandir(i).name,ffiles(1).name)));
    
        imheader = dicominfo(fullfile(datadir,scandir(i).name,ffiles(1).name));
        
        b = imheader.RescaleIntercept;
        m = imheader.RescaleSlope;
        
        im = im.* m + b;
        
        pixelx = imheader.PixelSpacing(1);
        pixely = imheader.PixelSpacing(2);
        pixelz = imheader.SliceThickness;
        
        if showfig > 2              %figure 1 original sample image to check data set is correct
            figure
            imagesc(im)
            colormap gray
            hold on
            title ('Original Sample Image')
            axis ('image');
            xlabel ('pixels');
            ylabel ('pixels');
            caxis ([-1000 1000]);
            
        end
        
        Nx = size(im,2);
        Ny = size(im,1);
        
        imaxisx = [1:Nx] .* pixelx;         %size of image in mm from no. of pixels x size of pixel
        imaxisy = [1:Ny] .* pixely;
        
        imavg = zeros(Ny,Nx);           %create imavg same size as im with zeros
        im3D = zeros(Ny,Nx,Nz);         %create im3D
    end
    
    % Load data into memory and calculate an average 2D image to later
    % determine phantom location
    
    for z = 1:Nz
        im = single(dicomread(fullfile(datadir,scandir(i).name,ffiles(z).name)));
        im = im.* m + b;
        imavg(:,:) = imavg + im;     %imavg defined as the sum of all images across all scans
    end
    
end

imavg = imavg./(Nz*Nscan);          %imavg defined as the average of all images across all scans


fprintf ('Select region for calculation carefully! \n');

h=imrect(gca, [250 305 125 125]);           %06/04/17 fixed size rectangle for user positioning to crop the image 
setResizable(h,0)
position = wait(h);


[subim, FOV] = imcrop(imavg,position);                   %crop image to reduce number of zeros to be dealt with later 


cropmask = size(subim);                  %create a mask from the crop size selected
imavg = subim;                           %redefine imavg as the cropped average image


imaxisx = [1:size(imavg,2)] .* pixelx;            %set sizes of cropped image
imaxisy = [1:size(imavg,1)] .* pixely;

if showfig > 3 || manualmask == 1       %figure 2 Average image
    
    figure
    imagesc(imavg)                 %imavg = cropped average of all scan image
    colormap gray
    title ('Average Image')
    hold on
    axis ('image');
    caxis ([-1000 1000]);
    xlabel ('pixels');
    ylabel ('pixels'); 
    impixelinfo;
end 
    
 
    
                                     % polygon roi on the mask image returns mask with 1 inside and 0 outside
mask = zeros(size(imavg)); 
choice = menu('Is a mask required?','Yes','No')         %asks user if a mask is required to hide other details or artefacts

if choice==1
    fprintf ('Select region of image to analyse.\n\n Double click closed vertex to create mask.\n\n');
    mask = roipoly;
else
    mask = ones(size(imavg));                           %otherwise use whole image
end   
                                        % Plot mask image overlying sample image to verify mask and position
if showfig > 2 || manualROI == 1        %figure 3 masked average image for background ROI placement
    figure
    imagesc((imavg+100).*mask)          %+100 needed due to masking function
    xlabel('pixel')
    ylabel('pixel')
    colormap gray
    hold on
    title ('Masked image for ROI placement') 
    axis image;
    impixelinfo;
end

% Determine background and detail ROI values to properly normalize the data to greyscale range 0-1

if manualROI == 1
    fprintf('Click and drag to create a rectangular ROI representing the background.  Double click the ROI when finished.\n\n');
    
    h = imrect;
    ptROI = wait(h);
    
    
    BGxROI1 = round(ptROI(1));
    BGxROI2 = BGxROI1 + round(ptROI(3));
    BGyROI1 = round(ptROI(2));
    BGyROI2 = BGyROI1 + round(ptROI(4));
    
    % force selected ROI to be within the bounds of the image
    BGxROI1 = min(BGxROI1,size(im,2));
    BGxROI2 = min(BGxROI2,size(im,2));
    BGyROI1 = min(BGyROI1,size(im,1));
    BGyROI2 = min(BGyROI2,size(im,1));
    BGxROI1 = max(BGxROI1,1);
    BGxROI2 = max(BGxROI2,1);
    BGyROI1 = max(BGyROI1,1);
    BGyROI2 = max(BGyROI2,1);
end

BGvalue = mean(mean(imavg(BGyROI1:BGyROI2,BGxROI1:BGxROI2)));


if manualROI == 1
    fprintf('Click and drag to create a rectangular ROI representing the detail.  Double click the ROI when finished.\n\n');
    
    h = imrect;
    ptROI = wait(h);
    
    FGxROI1 = round(ptROI(1));
    FGxROI2 = FGxROI1 + round(ptROI(3));
    FGyROI1 = round(ptROI(2));
    FGyROI2 = FGyROI1 + round(ptROI(4));
    
    % force selected ROI to be within the bounds of the image
    FGxROI1 = min(FGxROI1,size(im,2));
    FGxROI2 = min(FGxROI2,size(im,2));
    FGyROI1 = min(FGyROI1,size(im,1));
    FGyROI2 = min(FGyROI2,size(im,1));
    FGxROI1 = max(FGxROI1,1);
    FGxROI2 = max(FGxROI2,1);
    FGyROI1 = max(FGyROI1,1);
    FGyROI2 = max(FGyROI2,1);
end

FGvalue = mean(mean(imavg(FGyROI1:FGyROI2,FGxROI1:FGxROI2)));


if or(showfig > 2,manualROI ==1)                           %shows background/detail ROIs
    plot([BGxROI1,BGxROI2,BGxROI2,BGxROI1,BGxROI1],[BGyROI1,BGyROI1,BGyROI2,BGyROI2,BGyROI1],'r-')
    plot([FGxROI1,FGxROI2,FGxROI2,FGxROI1,FGxROI1],[FGyROI1,FGyROI1,FGyROI2,FGyROI2,FGyROI1],'r-')
    hold off
end

imavg = (imavg - BGvalue) ./ (FGvalue - BGvalue);       % Normalize averaged image such that values are between 0-1

if showfig > 2              %Print figure 4 showing the average normalised masked image to be used in the calculation
    figure
    imagesc(imavg)
    xlabel('mm')
    ylabel('mm')
    title('Averaged and normalized image')
    colormap gray
    hold on
    axis image;
    impixelinfo;
end

imavg = imavg.*mask;
   figure
    imagesc(imavg)              %figure 5 the image used in the calculation
    xlabel('mm')
    ylabel('mm')
    title('masked image for calculation')
    colormap gray
    hold on
    axis image;
    impixelinfo;
                                % Determine which pixels correspond to the inner detail

imlog = zeros(size(imavg));

%imlog = imbinarize(imavg);          %Convert image to binary based 
level = graythresh(imavg);           %imbinarize is the function used by newer versions of MATLAB
imlog = im2bw(imavg,level);

   figure
    imagesc(imlog)          %figure 6 thresholded image
    xlabel('mm')
    ylabel('mm')
    title('Thresholded masked image for detail location')
    colormap gray
    hold on
    axis image;
    impixelinfo;
% Calculate the center of the detail

fprintf('Calculating the center of the detail and determining detail-pixel locations.\n\n');

if exist('bwconncomp')==2               % bwconncomp used for 3D objects
    CC = bwconncomp(imlog,26);          %bwconncomp finds the objects in the image
    for i = 1:CC.NumObjects;
        Lobj(i) =  length(CC.PixelIdxList{1,i}) ;  %gives the size of the object found
    end
else
    [CC CCn] = bwlabel(imlog,8);        %otherwise bwlabel is used for 2d images as in the case of imavg
    for i = 1:CCn
        Lobj(i) = length(find(CC == i));
    end   
end

L = find(Lobj == max(Lobj));        %L is the maximum size of the object found

S = regionprops(CC,'Centroid');     %regionprops returns the requested properties of the component found by bwconncomp

c.y = S(L).Centroid(2);         %gives y coordinate of the centre
c.x = S(L).Centroid(1);         %gives x coordinate of the centre


%%% Performing the MTF calculation on a single image

  %  fprintf('Select the DICOM image to use for mtf calculation.\n\n');         %choose the scan to perform the mtf calculation on
  %  [imfile, impath] = uigetfile('*','Pick a DICOM image to use for mtf calculation',datadir);
   
  %     imcalc = single(dicomread(fullfile(impath,imfile)));
  %     imcalc = imcalc.* m + b;
 
  %     imcalc = (imcalc - BGvalue) ./ (FGvalue - BGvalue);             %normalise calculation image based on averge image background values
      
  %     subim = imcrop (imcalc , FOV);                       %crops the calculating image with the same FOV as outlined earlier
       
   
       
if showfig>2                                  %print figure 7 showing the detail hightlighted in red with the centre point marked
    figure
    imagesc(imavg)
    xlabel('mm')
    ylabel('mm')
    title('Identified detail with centre point')
    colormap gray
    hold on
    axis image;
    impixelinfo;
    if exist('bwconncomp')==2
        [I J] = ind2sub(size(imavg),CC.PixelIdxList{1,L});
    else
        [I,J] = find(CC == L);
    end
    plot(J,I,'.r')               %filling detail in red
    plot(c.x,c.y,'*y')            %plotting x and y coordinate of centre point in yellow x
    hold off
end

xaxis = ([1:size(imavg,2)] - c.x) .* pixelx;
yaxis = ([1:size(imavg,1)] - c.y) .* pixely;



% Convert all image coords to polar coords with origin at center of the
% detail


r = zeros(size(imavg));
th = zeros(size(imavg));

for i = 1 :size(imavg,2)
    for j = 1 : size(imavg,1)
       
        [th(j,i) r(j,i)] = cart2pol(xaxis(i),yaxis(j));
    end
end

% Create oversampled ESF excluding data specified in mask

rmax = max(max(r));

esf = zeros(ceil(rmax/rstep)+1,1);  %defining the ESF matrix at a size (number of bins rounded to the nearest 1)
nsamp = zeros(length(esf),1);

rbin = 0:rstep:rmax+rstep;   %set the bins for the ESF from 0 up in steps of rstep until rmax+rstep)
if showfig > 3              
    figure
    imagesc((imavg+1).*mask)            %figure 8 
    title('Completed during calculation, multicoloured due to display limitations')
    colormap gray
    hold all
    axis image;
    impixelinfo;
end

% data with radius falling within a given bin are averaged together for a
% low noise approximation of the ESF at the given radius

fprintf('Calculating the MTF.\n');
fprintf('The may take several minutes. \n');
fprintf('Please be patient.\n\n');



thlim1 = min(thlim1,thlim2);
thlim2 = max(thlim1,thlim2);

numbins = length(rbin);

h = waitbar (0, 'Calculating MTF');
steps = numbins;

for i = 1:numbins
    waitbar(i/numbins)
    %progressstr = ['Calculating annulus ',num2str(i),' of ',num2str(numbins),' \n'];
    %fprintf(progressstr);
    R1 = find(r >= rbin(i));            %find pixels at the radius of current bin
    R2 = find(r < rbin(i) + rstep);     %find pixels at radii less than current bin + one step
    R3 = intersect(R1,R2);
    R4 = find(th >= thlim1);            %find pixels around the full angle range applied
    R5 = find(th < thlim2);
    R6 = intersect(R5,R4);              %find pixels that are at the correct radius and angle range
    R7 = intersect(R3,R6);
    R8 = R7.*mask(R7);                  %apply the mask to discard masked pixels
    R=R8(R8~=0);                        %R is all relevant pixels that are not zeros
    if showfig > 3
        [X Y] = ind2sub(size(imavg),R);%what is this?********************************
        plot(Y,X,'.')
        hold all
    end
    esf(i) = sum(imavg(R));
    nsamp(i) = length(R);
end

close(h)

i1 = find(nsamp,1,'first');
i2 = find(nsamp,1,'last');
nsamp = nsamp(i1:i2);
esf = esf(i1:i2);
rbin = rbin(i1:i2);

I = find(nsamp > 0);
esf(I) = esf(I)./nsamp(I);


% Plot oversampled ESF
if showfig > 2
    figure
    plot(rbin,esf)              %figure 9
    xlabel('radius (mm)')
    ylabel('Normalized and oversampled ESF')
    title ('ESF')
    grid on
    axis([rbin(1) rbin(end) -1.2*min(esf) 1.1*max(esf)])
end


 % Calculated the LSF from the ESF
lsf = diff(esf);

                    % artifacts caused by empty bins likely have a value of -/+ 1, so try to remove them. 
I = find(abs(lsf)> 0.9);
lsf(I) = 0;

if max(lsf) < max(abs(lsf))
    lsf = -lsf;
end

maxlsf = max(lsf);

rcent = find(lsf == maxlsf);                     %centre of the lsf is at its peak
lsfaxis = rbin(1:end-1) - rbin(rcent);           %lsfaxis = radius 1 - radius at lsf peak

% Center edge location in LSF and keep surrounding data range specified by
% pad. 
pad = round(length(lsfaxis)/6);             %pad is the width of the LSF FOV. Naomi changed to divide by 7 not 5 (default was 5) 30-3-17 
lsfaxis = lsfaxis(rcent-pad:rcent+pad);     %The axis is the centre-pad to centre+pad
lsf = lsf(rcent-pad:rcent+pad);             %LSF displayed over the FOV range

nlsf = length(lsf);

%calculate Hann window and apply to data if specified.
if window == 1
    w = 0.5 .* (1 - cos(2*pi*[1:length(lsfaxis)]./length(lsfaxis)));
    w = w';
else
    w = ones(length(lsfaxis),1);
end
lsfw = lsf .* w;

% Plot windowed LSF and scaled window together
if showfig > 1
    figure
    plot(lsfaxis,lsfw)              %figure 10
    hold on
    plot(lsfaxis,w.*max(lsfw),'r--')
    hold off
    title ('LSF')
    legend('oversampled + normalized LSF','scaled Hann window')
    xlabel('radius (mm)')
    ylabel('signal')
    axis([lsfaxis(1) lsfaxis(end) 1.1*min(lsfw) 1.1*max(lsfw)])
    grid on
end

% Calculate the MTF from the LSF

T = fftshift(fft(lsfw));
faxis = ([1:nlsf]./nlsf - 0.5) ./ rstep;

MTF = abs(T);

%Plot the MTF
if showfig > 1
    figure
    plot(faxis,MTF,'.-')    %figure 11
    xlabel('spatial frequency (mm^{-1})');
    ylabel('MTF_r')
    title ('MTF')
    axis([0 max(faxis) 0 1.1*max(MTF)])
    grid on
end

fprintf('MTF calculated.\n\n');

% Save MTF results if indicated
if saveresults == 1
    cd(resultsdir);
results = '123.xlsx';
mtfloc = 'B3';          %location of start of MTF data
axisloc = 'A2';         %location of start of axis data
    save(fullfile(resultsdir,'axisvalues.txt'),'faxis','-ASCII')
    xlswrite(results,faxis,results,axisloc);  %save axis values into the results spreadsheet
    save(fullfile(resultsdir,'MTFvalues.txt'),'MTF','-ASCII')
    xlswrite(results,MTF,results,mtfloc);   %save the mtf values into the results spreadsheet
    fprintf('Results saved.\n\n');
end

return;
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
