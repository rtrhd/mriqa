{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Quarterly QA on Siemens Scanners"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is performed on the Siemens \"long bottle\" phantom in the most used head coil - either the standard Head and Neck 25 element coil or the 32 channel head coil. These are used for the Siemens coil QA and it can be easily and reprodicibly positioned using the dedicated Siemens foam pad.\n",
    "\n",
    "Where this phantom is not available (eg on Avanto generation systems) then the slightly wider \"short bottle\" and its corresponding holder should be used. This variant may be specified in the analysis below.\n",
    "\n",
    "The bandwidth used should be approximately scaled with the field strength so 130Hz at 1.5T and 260Hz at 3.0T."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The quarterly QA scans consist of the following scans:\n",
    " - SE, 2 acquisitions, with/without PSN, multiple elements, RMS coil combine -> SNR, Unif, XY gradient scale\n",
    " - SE, 2 acquisitions, without PSN, multiple elements, RMS coil combine, TX switched off by setting reference voltages to zero\n",
    " - EPI, 60+ acquisitions, ghosting and long term stability, eddy currents and shim (distortion)\n",
    "\n",
    "The acquistions should have the following labels in the protocol to make them easier to identify:\n",
    " - `QQA_SE_SIGNAL`\n",
    " - `QQA_SE_NOISE`\n",
    " - `QQA_EPI_STABILITY`\n",
    " \n",
    "Although we may well have multiple series for each label we should be able to deduce which is which based on the image type, coil names and coil element masks."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Boiler plate"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from __future__ import division, print_function\n",
    "\n",
    "%matplotlib inline\n",
    "import matplotlib.pyplot as plt\n",
    "plt.style.use('seaborn')\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "\n",
    "from mriqa.phantoms import SIEMENSLONGBOTTLE, SIEMENSSHORTBOTTLE\n",
    "from mriqa.dcmio import (\n",
    "    fetch_series, coil_elements,\n",
    "    larmor_frequency, gradient_sensitivities, transmitter_calibration, tales_reference_power\n",
    ")\n",
    "from mriqa.tools import show_montage\n",
    "\n",
    "from dcmextras.siemenscsa import csa, phoenix\n",
    "\n",
    "from mriqa.reports import (\n",
    "    ghosting_report, uniformity_report, noise_correlation_report,\n",
    "    circularity_report, snr_report_multi\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Details of series to analyse."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "patid = 'PQACRK20161215'\n",
    "studyid = '1'\n",
    "series = {\n",
    "    'Signal': [26, 30],\n",
    "    'Signal_Combined': [27, 31],\n",
    "    'Noise': [22, 24],\n",
    "    'Noise_Combined': [23, 25],\n",
    "    'Geometry': [28, 32]\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Individual coil element images"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Assume just one element\n",
    "dobjs = sorted(\n",
    "    fetch_series(patid, studyid, sernos=series['Signal']),\n",
    "    key=lambda x: (int(x.SeriesNumber), int(x.AcquisitionNumber), coil_elements(x)[0])\n",
    ")\n",
    "nchannels = len(set(coil_elements(d)[0] for d in dobjs))\n",
    "dobjsa, dobjsb = dobjs[:nchannels], dobjs[nchannels:]\n",
    "\n",
    "show_montage(dobjsa, dobjsb, op='mean', cmap='viridis', title='Per-channel Signal Images');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Coil Element Noise Images (Zero TX Voltage)\n",
    "A more rigorous way of investigating the noise is to acquire images with no signal at all. We have set the TX voltage to zero in the System part of the UI. Unfortunately, this is not persistent in the protocol so has to be done each time it is run.\n",
    "\n",
    "These are the individual noise images. We'll assume that the noise is spatially stationary in each of these images and that, furthermore, it remains so when they are combined as sum-of-squares."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dobjs = sorted(\n",
    "    fetch_series(patid, studyid, sernos=series['Noise']),\n",
    "    key=lambda x: (int(x.SeriesNumber), int(x.AcquisitionNumber), coil_elements(x)[0])\n",
    ")\n",
    "nchannels = len(set(coil_elements(d)[0] for d in dobjs))\n",
    "dobjsa, dobjsb = dobjs[:nchannels], dobjs[nchannels:]\n",
    "show_montage(dobjsa, dobjsb, op='diff', cmap='coolwarm', title='Per-channel Noise Difference Images');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Noise Image Channel Covariance (Crosstalk)\n",
    "We'll look at the any strong variation in the coil element noise powers that might indicate a broken element. In addition, any significant off-diagonal power indicates an unwanted coupling between the elements.\n",
    "\n",
    "Ideally we would like to calculate this covariance using the raw complex data. Here we have only the magnitude image data, so for now we'll assume that the covariance matrix is still representative if we take the *difference* of two images for each element."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "noise_correlation_report(dobjsa, dobjsb).T"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Signal to Noise Ratio"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Signal to noise ratio using uncombined signal images, which we combine here as some of squares. A current limitation is that this may only work with Siemens where we have multi-series not multi-frames."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# SNR (uncombined channels)\n",
    "dobjs = sorted(\n",
    "    fetch_series(patid, studyid, sernos=series['Signal']),\n",
    "    key=lambda x: (int(x.SeriesNumber), int(x.AcquisitionNumber), coil_elements(x)[0])\n",
    ")\n",
    "nchannels = len(set(coil_elements(d)[0] for d in dobjs))\n",
    "dobjsa, dobjsb = dobjs[:nchannels], dobjs[nchannels:]\n",
    "\n",
    "snr_report_multi(dobjsa, dobjsb, phantom=SIEMENSLONGBOTTLE, coil='HEAD32')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "-------"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Geometry\n",
    "For geometrical measures we want to look at the average of the coil-combined images with *Prescan Normalise* intensity correction applied. This will give us the highest SNR and the most uniform image, which should make the segmentation more robust wthout affecting the image geometry."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Phantom Circularity (Scale and Distortion)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dobjs = sorted(\n",
    "    fetch_series(patid, studyid, sernos=series['Geometry']),\n",
    "    key=lambda x: (int(x.SeriesNumber), int(x.AcquisitionNumber))\n",
    ")\n",
    "circularity_report(dobjs[0], phantom=SIEMENSLONGBOTTLE).T"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "-------------"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Ghosting\n",
    "We can use the same routines for ghosting as for the small GE sphere. As the phantom is cylindrical, of course, only the axial plane will be considered.\n",
    "\n",
    "Note that the actual values obtained for the ghosting ratio will vary somewhat according to exactly where the ghosting regions of interest are defined. In particular, any ringing-like behaviour will only be detected with a ROI close to the phantom. Nyquist ghosting on the other hand is best detected with an ROI that comprises the phantom shifted by N/2 in the *phase encoding* direction.\n",
    "\n",
    "As the test compares background signal in the phase encoding and the readout direction we are only sensitive to these separately (generally in the phase encoding direction). Anything that is manifest on both axes will cancel out in the subtraction. Possibly a better point of comparison would be the image corners."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# LONGBOTTLE: Ghosting\n",
    "dobjs = fetch_series(patid, studyid, sernos=series['Signal_Combined'])\n",
    "results = ghosting_report([dobjs[0]], phantom=SIEMENSLONGBOTTLE)\n",
    "results.T"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "----"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Uniformity\n",
    "These are the same routines for uniformity as for the oil sphere used at acceptance but the phantom is cylindrical so only the axial plane is considered. Note that, at 3T there will be a significant contribution from the $B_1^+$ non-uniformity as well as from the receive coil sensitivities. Only some of this may be compensated for by the prescan normalize option; the scans used to calibrate this may have different sensitivity to flip angle variations. On the other hand this is quite a small phantom so the $B_1^+$ effects may not be very pronounced over such a limited volume."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "raw_dobjs = fetch_series(patid, studyid, sernos=series['Signal_Combined'])\n",
    "psn_dobjs = fetch_series(patid, studyid, sernos=series['Geometry'])\n",
    "df = uniformity_report(raw_dobjs=raw_dobjs[:1], psn_dobjs=psn_dobjs[:1], phantom=SIEMENSLONGBOTTLE)\n",
    "df[['XUniformityRaw', 'YUniformityRaw', 'NEMAUniformityRaw']].T"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df[['XUniformityNorm', 'YUniformityNorm', 'NEMAUniformityNorm']].T"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Frequency Reference and Transmitter Voltage\n",
    "These are the reference values reported in the DICOM headers."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dobj = raw_dobjs[0]\n",
    "sensitivities = 1e6 * np.array(gradient_sensitivities(dobj))\n",
    "pd.DataFrame({\n",
    "    'ImagingFrequency': [larmor_frequency(dobj)],\n",
    "    'TransmitterCalibration': [transmitter_calibration(dobj)],\n",
    "    'TalesReferencePower': [tales_reference_power(dobj)],\n",
    "    'GradientSensitivityX': [sensitivities[0]],\n",
    "    'GradientSensitivityY': [sensitivities[1]],\n",
    "    'GradientSensitivityZ': [sensitivities[2]]\n",
    "}, index=['Value']).T"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.11"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
