{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# MRI QA Summary Reports"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "from __future__ import division, print_function\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "from IPython.display import display\n",
    "import seaborn as sns\n",
    "\n",
    "from mriqa.dcmio import fetch_series\n",
    "from mriqa.reports import ghosting_report, slice_profile_report, distortion_report, resolution_bars_report, uniformity_report"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Slice Profile"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The TO2 phantom has two crossed plates and two pairs of wedges. Here we use the two crossed plates to estimate the full-width half maximum of the slice profile for 5mm and 3mm slices as described in the IPEM *green book*.\n",
    "\n",
    "To go from the width of the profile in the image we need to scale by the tangent of the plate angle which here we take to be 11.7 degrees.\n",
    "\n",
    "To facilitate the analysis we interpolate the images to higher resolution first. Having extracted the thick profiles we fit a quadratic bias the the start and end of the profile assuming this is outside of the plate and is just the gain field for the background liquid in the phantom. We divide the profiles by this modelled gain variation and also invert them to give a positive going *line shape*.\n",
    "\n",
    "This is fitted using cubic splines and the half height position found to determine the FWHM which is then scaled appropriately for pixel size and plate projection.\n",
    "\n",
    "Although we correct for it below, it is better to use prescan normalised images to limit the bias in the background field."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3 mm Slice"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# TO2: Slice 3mm\n",
    "patid = 'PQA20150331RHD'\n",
    "stuid = '1'\n",
    "\n",
    "# Distortion Corrected, Phase RL, Uniformity Corrected 512*512\n",
    "dobjs = fetch_series(patid, stuid, sernos=[10])\n",
    "slice_idx = 1\n",
    "results = slice_profile_report(dobjs[slice_idx], flipped_ud=False, flipped_rl=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 5 mm Slice"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# TO2: Slice 5mm\n",
    "patid = 'PQA20150331RHD'\n",
    "stuid = '1'\n",
    "\n",
    "# Distortion Corrected, Phase RL, Uniformity Corrected 512*512\n",
    "dobjs = fetch_series(patid, stuid, sernos=[6])\n",
    "slice_idx = 1\n",
    "result = slice_profile_report(dobjs[slice_idx], flipped_ud=False, flipped_rl=False)\n",
    "results = results.append(result)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "results[['FWHMGMeanPixels', 'FWHMGMeanMM']]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Geometric Distortion and Scaling"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The TO2 phantom has a 120mm square box for distortion measurements. Here we use this to estimate the in plane scale (aka *linearity*) and the relative distortion as described in the IPEM Report 80 *green book*. It is best to use the thinner 3mm slice series for this to minimise the blurring from misalignment. We choose a slice in the middle of the block.\n",
    "\n",
    "In addition, if scans are available at 512x512 rather than 256x256 this will allow more accurate determination of the distortion but note that in this case the bandwidth should be scaled down by a factor of two so as to obtain the same sensitivity to off-resonance distortion. The scans should be acquired with the phase encoding direction both along columns and along rows to separate off-resonance effects from gradient spatial non-linearity.\n",
    "\n",
    "After centring the phantom we extract an ROI for the box using its expected position in the phantom. The central area is occupied by the wedges and plates so we fill this with the phantom background intensity so it doesn't interfere with profiles across the ROI.\n",
    "\n",
    "Then, to get smoother and more easily locatable peaks in the profiles, we interpolate the image to higher resolution using the `scipy.ndimage` image `zoom` function. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# TO2: Distortion\n",
    "# SE: Axial, Phase RL, 512 matrix\n",
    "patid = 'PQA20150331RHD'\n",
    "stuid = '1'\n",
    "dobjs = fetch_series(patid, stuid, sernos=[8])\n",
    "\n",
    "slice_idx = len(dobjs)//2\n",
    "results = distortion_report(dobjs[slice_idx], flipped_ud=False, flipped_rl=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# TO2: Distortion\n",
    "# SE: Axial, Phase AP, 512 matrix\n",
    "patid = 'PQA20150331RHD'\n",
    "stuid = '1'\n",
    "dobjs = fetch_series(patid, stuid, sernos=[12])\n",
    "\n",
    "slice_idx = len(dobjs)//2\n",
    "results = results.append(distortion_report(dobjs[slice_idx], flipped_ud=False, flipped_rl=False))\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "results[['Orientation', 'PhaseDirection', 'Sensitivity', 'HorizontalScale', 'HorizontalDistortion','VerticalScale', 'VerticalDistortion' ]]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Spatial Resolution"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The resolution is assessed using TO4 which has parallel plates of various separations as well as MTF blocks. Here we'll just use the plates. As it is very difficult to align the phantom so that the plates are exactly along a pixel axis we automatically realign the images before projecting along the plate direction to give an average profile across the plates."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# TO4: Resolution\n",
    "patid = 'PQA20140807SC'\n",
    "stuid = '3'\n",
    "\n",
    "dobjs = fetch_series(patid, stuid, sernos=[85])\n",
    "slice_idx = 0\n",
    "result = resolution_bars_report(dobjs[slice_idx], flipped_ud=False, flipped_rl=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Ghosting"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To test for image ghosting we follow the approach of the AAPM report and look for image intensity outside a symmetrically positioned phantom. This is sensitive to Nyquist ghosting in EPI scans and scattered ghosts due to phase instabilities in fast spin echo. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Spin Echo"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# GE10CM: Ghosting\n",
    "patid = 'PQA20150309CRIC'\n",
    "stuid = '1'\n",
    "# SE Axial\n",
    "dobjs = [fetch_series(patid, stuid, sernos=[serno])[0] for serno in [14, 16]]\n",
    "results = ghosting_report(dobjs)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Fast Spin Echo"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# GE10CM: Ghosting\n",
    "# TSE18 Axial\n",
    "dobjs = [fetch_series(patid, stuid, sernos=[serno])[0] for serno in [18, 20]]\n",
    "results = results.append(ghosting_report(dobjs))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### EPI"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# GE10CM: Ghosting\n",
    "# EPIFID64 Axial\n",
    "dobjs = [fetch_series(patid, stuid, sernos=[serno])[0] for serno in [21, 22]]\n",
    "results = results.append(ghosting_report(dobjs))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Spin Echo EPI"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# GE10CM: Ghosting\n",
    "# EPISE128 Axial\n",
    "dobjs = [fetch_series(patid, stuid, sernos=[serno])[0] for serno in [24, 26]]\n",
    "results = results.append(ghosting_report(dobjs))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "display(results[['Sequence', 'Orientation', 'PhaseDirection', 'GhostRatio']])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Receiver Coil Uniformity"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We test the uniformity of the head coil (either the standard coil or the advanced neuro dpending on what is normally used). We use a silicone oil filled spherical phantom so as to avoid B<sub>1+</sub> shading effects at 3T. We obtain images both with and without prescan based intensity normalisation (this can be done in a single acquisition on Siemens systems). The analysis is based on the green book method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# OILSPHERE: Uniformity\n",
    "# SE Axial,Coronal,Sagittal; w w/o Intensity Normalization\n",
    "patid = 'PQA20150309CRIC'\n",
    "stuid = '1'\n",
    "\n",
    "raw_tra = fetch_series(patid, stuid, sernos=[3])\n",
    "psn_tra = fetch_series(patid, stuid, sernos=[4])\n",
    "raw_sag = fetch_series(patid, stuid, sernos=[5])\n",
    "psn_sag = fetch_series(patid, stuid, sernos=[6])\n",
    "raw_cor = fetch_series(patid, stuid, sernos=[7])\n",
    "psn_cor = fetch_series(patid, stuid, sernos=[8])\n",
    "\n",
    "if len(raw_tra) > 1:\n",
    "    results = uniformity_report(\n",
    "        raw_dobjs=[raw_tra[0], raw_sag[0], raw_cor[0]],\n",
    "        psn_dobjs=[psn_tra[0], psn_sag[0], psn_cor[0]],\n",
    "        raw_dobjsb=[raw_tra[1], raw_sag[1], raw_cor[1]],\n",
    "        psn_dobjsb=[psn_tra[1], psn_sag[1], psn_cor[1]]\n",
    "    )\n",
    "else:\n",
    "    results = uniformity_report(\n",
    "        raw_dobjs=[raw_tra[0], raw_sag[0], raw_cor[0]],\n",
    "        psn_dobjs=[psn_tra[0], psn_sag[0], psn_cor[0]],\n",
    "    )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "print('Raw Images')\n",
    "display(results[['SeriesRaw',  'ProtocolRaw',  'OrientRaw', 'XUniformityRaw', 'YUniformityRaw']])\n",
    "print('Intensity Normalised Images')\n",
    "display(results[['SeriesNorm', 'ProtocolNorm', 'OrientNorm', 'XUniformityNorm', 'YUniformityNorm']])"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.11"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
