{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# QA Phantom Analysis"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from __future__ import division\n",
    "from mriqa.phantoms import phantom_mask_2d, find_phantom, TO2\n",
    "from mriqa.dcmio import fetch_series\n",
    "from mriqa.tools import mean_im, diff_im, snr_im, snr, show_mosaic\n",
    "from skimage.exposure import rescale_intensity\n",
    "from scipy.ndimage.interpolation import zoom\n",
    "import numpy as np\n",
    "from matplotlib.pyplot import subplots, Rectangle\n",
    "import seaborn as sns"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The TO2 phantom has a 120mm square box for distortion measurements. Here we use this to estimate the in plane scale (aka *linearity*) and the relative distortion as described in the IPEM *green book*. We use the 3mm slice series for this and we choose a slice in the middle of the block. \n",
    "\n",
    "After centring the phantom we extract an ROI for the box using its expected position in the phantom. The central area is occupied by the wedges and plates so we fill this with the phantom background intensity so it doesn't interfere with profiles acorss othe ROI.\n",
    "\n",
    "Then, to get smoother and more easily locatable peaks in the profiles we interpolate the image to higher resolution using the `scipy` image `zoom` function. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Distortion on TO2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "def report_distortion(dobj, frame=None, flipped_ud=False, flipped_rl=False):\n",
    "    '''Takes a dicom object which is a single slice around the middle of the phantom TO2.\n",
    "       Plots the image with the distortion box positions marked on, the extracted ROI with the profiles marked on\n",
    "       and the estinated values for the image scaling and distortion based on the 120mm square in TO2.\n",
    "    '''\n",
    "    # Get image data and reorient if need be\n",
    "\n",
    "    # Handle multiframes\n",
    "    if 'NumberofFrames' in dobj and dobj.NumberofFrames > 1:\n",
    "        if frame is None:\n",
    "            frame = dobj.NumberofFrames//2\n",
    "        image = dobj.pixel_array[frame] & 0xfff\n",
    "        pixel_spacing = dobj.PerframeFunctionalGroups[0].PixelMeasures[0].PixelSpacing\n",
    "        slice_thickness = dobj.PerframeFunctionalGroups[0].PixelMeasures[0].SliceThickness\n",
    "    else:\n",
    "        pixel_spacing = dobj.PixelSpacing\n",
    "        slice_thickness = dobj.SliceThickness\n",
    "        image = dobj.pixel_array & 0xfff\n",
    "\n",
    "    if flipped_ud:\n",
    "        image = np.flipud(image)\n",
    "    if flipped_rl:\n",
    "        image = np.fliplr(image)\n",
    "\n",
    "    pix_dims = np.asarray(pixel_spacing)\n",
    "    \n",
    "    # Plotting area\n",
    "    fig, axes = subplots(1, 2, figsize=(16, 8))\n",
    "    fig.subplots_adjust(wspace=0.5)\n",
    "\n",
    "    # Show phantom image\n",
    "    axes[0].imshow(image, cmap='bone')\n",
    "    axes[0].axis('off')\n",
    "    axes[0].axis('image')\n",
    "    \n",
    "    # Find centre of phantom\n",
    "    expected_radius = TO2['Diameter'] / 2 / pix_dims[0]\n",
    "    centre_x, centre_y, radius = find_phantom(image, expected_radius=expected_radius)\n",
    "\n",
    "    # Positions of distortion box\n",
    "    box     = TO2['Features']['Boxes'][0]\n",
    "    boxsize = TO2['FeatureSizes']['Boxes'][0]\n",
    "\n",
    "    # Outer bounding rectangle for distortion box\n",
    "    (x, y), (dx, dy) = np.array(box) / pix_dims\n",
    "    x += centre_x\n",
    "    y += centre_y\n",
    "    axes[0].add_artist(Rectangle([x, y], dx, dy, fill=False, edgecolor='r'))\n",
    "    \n",
    "    # Inner bounding rectangle for distortion box\n",
    "    (x, y), (dx, dy) = (np.array(box) + [[boxsize, boxsize], [-2*boxsize, -2*boxsize]]) / pix_dims\n",
    "    x += centre_x\n",
    "    y += centre_y\n",
    "    axes[0].add_artist(Rectangle([x, y], dx, dy, fill=False, edgecolor='r'))\n",
    "\n",
    "    # Outer bounding rectangle for distortion box to select ROI\n",
    "    (x, y), (dx, dy) = np.array(box) / pix_dims\n",
    "    x += centre_x\n",
    "    y += centre_y\n",
    "    box_roi = image[y:y+dy+1,x:x+dx+1].copy()\n",
    "\n",
    "    # Inner bounding rectangle for distortion box to foreground fill\n",
    "    margin_x, margin_y = np.array([boxsize, boxsize]) / pix_dims\n",
    "    fill_value = (\n",
    "        np.mean(box_roi[ margin_y, margin_x:-margin_x]) +\n",
    "        np.mean(box_roi[-margin_y, margin_x:-margin_x]) +\n",
    "        np.mean(box_roi[margin_y:-margin_y,  margin_x]) +\n",
    "        np.mean(box_roi[margin_y:-margin_y, -margin_x])\n",
    "    ) / 4\n",
    "    box_roi[margin_y:-margin_y, margin_x:-margin_x] = fill_value\n",
    "\n",
    "    # Interpolate up for smooth profiles\n",
    "    zoom_factor = 4\n",
    "    box_roi = zoom(box_roi, zoom_factor)\n",
    "    pix_dims = pix_dims / zoom_factor\n",
    "\n",
    "    # Show Region of Interest\n",
    "    axes[1].imshow(box_roi, cmap='bone')\n",
    "    axes[1].axis('off')\n",
    "    axes[1].axis('image')\n",
    "\n",
    "    # Horizontal profiles (inverted)\n",
    "    row_h1 = box_roi.shape[0]//4\n",
    "    profile_h1 = np.mean(box_roi[row_h1-1:row_h1+2,:], axis=0)\n",
    "    profile_h1 = profile_h1[len(profile_h1)//2] - profile_h1\n",
    "\n",
    "    row_h2 = box_roi.shape[0]//2\n",
    "    profile_h2 = np.mean(box_roi[row_h2-1:row_h2+2,:], axis=0)\n",
    "    profile_h2 = profile_h2[len(profile_h2)//2] - profile_h2\n",
    "\n",
    "    row_h3 = 3 * box_roi.shape[0]//4\n",
    "    profile_h3 = np.mean(box_roi[row_h3-1:row_h3+2,:], axis=0)\n",
    "    profile_h3 = profile_h3[len(profile_h3)//2] - profile_h3\n",
    "\n",
    "    # Overlay horizontal profiles on ROI image\n",
    "    im_height = box_roi.shape[0]\n",
    "    profile_height = np.max(profile_h1)\n",
    "    axes[1].plot(row_h1 - profile_h1/profile_height * im_height/10)\n",
    "    axes[1].plot(row_h2 - profile_h2/profile_height * im_height/10)\n",
    "    axes[1].plot(row_h3 - profile_h3/profile_height * im_height/10)\n",
    "\n",
    "    # Horizontal Separations\n",
    "    profile_len = len(profile_h1)\n",
    "    d_h1a = np.argmax(profile_h1[:profile_len//4])\n",
    "    d_h1b = np.argmax(profile_h1[-profile_len//4:])\n",
    "    dist_h1_pix = (d_h1b + profile_len - profile_len//4 - d_h1a)\n",
    "    dist_h1_mm = dist_h1_pix * pix_dims[1]\n",
    "\n",
    "    d_h2a = np.argmax(profile_h2[:profile_len//4])\n",
    "    d_h2b = np.argmax(profile_h2[-profile_len//4:])\n",
    "    dist_h2_pix = (d_h2b + profile_len - profile_len//4 - d_h2a)\n",
    "    dist_h2_mm = dist_h2_pix * pix_dims[1]\n",
    "\n",
    "    d_h3a = np.argmax(profile_h3[:profile_len//4])\n",
    "    d_h3b = np.argmax(profile_h3[-profile_len//4:])\n",
    "    dist_h3_pix = (d_h3b + profile_len - profile_len//4 - d_h3a)\n",
    "    dist_h3_mm = dist_h3_pix * pix_dims[1]\n",
    "\n",
    "    # Vertical profiles (inverted)\n",
    "    col_v1 = box_roi.shape[1]//4\n",
    "    profile_v1 = np.mean(box_roi[:, col_v1-1:col_v1+2], axis=1)\n",
    "    profile_v1 = profile_v1[len(profile_v1)//2] - profile_v1\n",
    "\n",
    "    col_v2 = box_roi.shape[1]//2\n",
    "    profile_v2 = np.mean(box_roi[:, col_v2-1:col_v2+2], axis=1)\n",
    "    profile_v2 = profile_v2[len(profile_v2)//2] - profile_v2\n",
    "\n",
    "    col_v3 = 3*box_roi.shape[1]//4\n",
    "    profile_v3 = np.mean(box_roi[:, col_v3-1:col_v3+2], axis=1)\n",
    "    profile_v3 = profile_v3[len(profile_v3)//2] - profile_v3\n",
    "\n",
    "    # Overlay vertical profiles on ROI image\n",
    "    im_width = box_roi.shape[1]\n",
    "    profile_height = np.max(profile_v1)\n",
    "    axes[1].plot(col_v1 + profile_v1/profile_height * im_width/10, range(len(profile_v1)))\n",
    "    axes[1].plot(col_v2 + profile_v2/profile_height * im_width/10, range(len(profile_v2)))\n",
    "    axes[1].plot(col_v3 + profile_v3/profile_height * im_width/10, range(len(profile_v3)))\n",
    "\n",
    "    # Vertical Separations\n",
    "    profile_len = len(profile_v1)\n",
    "    d_v1a = np.argmax(profile_v1[:profile_len//4])\n",
    "    d_v1b = np.argmax(profile_v1[-profile_len//4:])\n",
    "    dist_v1_pix = (d_v1b + profile_len - profile_len//4 - d_v1a)\n",
    "    dist_v1_mm = dist_v1_pix * pix_dims[0]\n",
    "\n",
    "    d_v2a = np.argmax(profile_v2[:profile_len//4])\n",
    "    d_v2b = np.argmax(profile_v2[-profile_len//4:])\n",
    "    dist_v2_pix = (d_v2b + profile_len - profile_len//4 - d_v2a)\n",
    "    dist_v2_mm = dist_v2_pix * pix_dims[0]\n",
    "\n",
    "    d_v3a = np.argmax(profile_v3[:profile_len//4])\n",
    "    d_v3b = np.argmax(profile_v3[-profile_len//4:])\n",
    "    dist_v3_pix = (d_v3b + profile_len - profile_len//4 - d_v3a)\n",
    "    dist_v3_mm = dist_v3_pix * pix_dims[0]\n",
    "\n",
    "    dists_h_mm = [dist_h1_mm, dist_h2_mm, dist_h3_mm]\n",
    "    dists_v_mm = [dist_v1_mm, dist_v2_mm, dist_v3_mm]\n",
    "    mean_h    =  np.mean(dists_h_mm)\n",
    "    mean_v    =  np.mean(dists_v_mm)\n",
    "    std_h      =  np.std(dists_h_mm)\n",
    "    std_v      =  np.std(dists_v_mm)\n",
    "\n",
    "    print 'Horizontal Distances:    ', dists_h_mm, '(mm)'\n",
    "    print 'Scale (Horizontal):       %0.1f mm' % mean_h\n",
    "    print 'Distortion (Horizontal):  %0.2f %%' % (100 * std_h / mean_h)\n",
    "    print 'Vertical Distances:      ', dists_v_mm, '(mm)'\n",
    "    print 'Scale (Vertical):         %0.1f mm' % mean_v\n",
    "    print 'Distortion (Vertical):    %0.2f %%' % (100 * std_v / mean_v)\n",
    "\n",
    "    return"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# TO2\n",
    "patid = 'PQA20140807SC'\n",
    "stuid = '3'\n",
    "sernos = [68]\n",
    "dobjs = fetch_series(patid, stuid, sernos)\n",
    "slice_ = len(dobjs)//2\n",
    "dobj = dobjs[slice_]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Horizontal Distances:     [120.60546875, 120.60546875, 120.60546875] (mm)\n",
      "Scale (Horizontal):       120.6 mm\n",
      "Distortion (Horizontal):  0.00 %\n",
      "Vertical Distances:       [120.849609375, 120.849609375, 120.7275390625] (mm)\n",
      "Scale (Vertical):         120.8 mm\n",
      "Distortion (Vertical):    0.05 %\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "/usr/local/anaconda/lib/python2.7/site-packages/ipykernel/__main__.py:60: VisibleDeprecationWarning: using a non-integer number instead of an integer will result in an error in the future\n",
      "/usr/local/anaconda/lib/python2.7/site-packages/ipykernel/__main__.py:67: VisibleDeprecationWarning: using a non-integer number instead of an integer will result in an error in the future\n",
      "/usr/local/anaconda/lib/python2.7/site-packages/ipykernel/__main__.py:68: VisibleDeprecationWarning: using a non-integer number instead of an integer will result in an error in the future\n",
      "/usr/local/anaconda/lib/python2.7/site-packages/ipykernel/__main__.py:70: VisibleDeprecationWarning: using a non-integer number instead of an integer will result in an error in the future\n"
     ]
    }
   ],
   "source": [
    "report_distortion(dobj, flipped_ud=False, flipped_rl=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "'''\n",
    "strip_a = roi_zoomed[:roi_zoomed.shape[0]//5,roi_zoomed.shape[1]//15:-roi_zoomed.shape[1]//15]\n",
    "strip_a = np.max(strip_a) - strip_a\n",
    "#imshow(strip, cmap='bone')\n",
    "edge_posn_a = np.argmax(strip_a, axis=0) * pix_dims_zoomed[0]\n",
    "location_a = arange(len(edge_posn_a)) * pix_dims_zoomed[1]\n",
    "plot(location_a, edge_posn_a, label='top' )\n",
    "\n",
    "strip_b = roi_zoomed[-roi_zoomed.shape[0]//5:,roi_zoomed.shape[1]//15:-roi_zoomed.shape[1]//15]\n",
    "strip_b = np.max(strip_b) - strip_b\n",
    "#imshow(strip, cmap='bone')\n",
    "edge_posn_b = (np.argmax(strip_b, axis=0) + roi_zoomed.shape[0] - roi_zoomed.shape[0]//5) * pix_dims_zoomed[0] - 120\n",
    "location_b = arange(len(edge_posn_b)) * pix_dims_zoomed[1]\n",
    "plot(location_b, edge_posn_b, label='bottom')\n",
    "xlabel('mm')\n",
    "ylabel('mm')\n",
    "legend();\n",
    "'''\n",
    "pass"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we detect the corner positions and go from their centroid, we then need to take account of any rotation, then we want the\n",
    "predicted corner positions. Then for each side we can plot deviation along the edge. That gives us absolute distortion on the boundary.\n",
    "\n",
    "The we could do scaling error horizontal and vertical by just taking the differences.\n",
    "\n",
    "We'd have six plots: distortion errors L,R,T,B; scale error V,H. We could then quote the worst in mm."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python [default]",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
